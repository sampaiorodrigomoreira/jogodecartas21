package br.com.itau;

public enum Naipe {

    COPAS("copas"),
    ESPADA("espada"),
    OURO("ouro"),
    PAUS("paus");

    public String naipe;

    Naipe(String naipe) {
        this.naipe = naipe;
    }
}
