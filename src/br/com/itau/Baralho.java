package br.com.itau;

public enum Baralho {

    DAMA(10),VALETE(10),REIS(10),
    AS(1), DOIS(2), TRES(3),
    QUATRO(4), CINCO(5), SEIS(6),
    SETE(7), OITO(8), NOVE(9), DEZ(10);

    public int valorCarta;

    Baralho(int valorCarta) {
        this.valorCarta = valorCarta;
    }
}
