package br.com.itau;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class IO {

    public static void processaJogo(Carta carta, Carta carta2){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Primeira carta: " + carta.getCarta() + " de " + carta.getNaipe());
        System.out.println("Segunda carta: " + carta2.getCarta() + " de " + carta2.getNaipe());

        int acumulador = carta.getCarta().valorCarta + carta2.getCarta().valorCarta;

        System.out.println("A soma do seu jogo é: " + acumulador);
        System.out.println(" Informe 1 = continuar " + "\n" +
                            "2 = Finalizar");
        int acao = scanner.nextInt();

        if(acao==1){

            acumulaResultado(acumulador);

        }else{

            System.out.println("Jogo finalizado");
        }

    }

    public static void acumulaResultado(int acumula){

        CriarBaralho retorna = new CriarBaralho();

        List<Carta> lista = retorna.retornaLista();
        CriarBaralho inicia = new CriarBaralho();
        Carta novaCarta1 = inicia.sortearCarta(lista);
        Carta novaCarta2 = inicia.sortearCarta(lista);

        IO.processaJogo(novaCarta1, novaCarta2);

    }

}


