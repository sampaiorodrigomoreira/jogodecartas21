package br.com.itau;

public class Carta {

    public Baralho carta;
    public Naipe naipe;

    public Carta(Baralho baralho, Naipe naipe) {
        this.carta = baralho;
        this.naipe = naipe;
    }

    public Baralho getCarta() {
        return carta;
    }

    public void setCarta(Baralho carta) {
        this.carta = carta;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }
}
