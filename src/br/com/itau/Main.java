package br.com.itau;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int opcao = Integer.parseInt(JOptionPane.showInputDialog(null,"1 = Iniciar Jogo 2 = Finalizar"));

        if(opcao==1){

            CriarBaralho retorna = new CriarBaralho();

            List<Carta> lista = retorna.retornaLista();

            CriarBaralho inicia = new CriarBaralho();

            Carta carta1 = inicia.sortearCarta(lista);
            Carta carta2 = inicia.sortearCarta(lista);

            IO.processaJogo(carta1, carta2);

        }else{

            JOptionPane.showMessageDialog(null, "Fim do jogo");

        }

    }
}
