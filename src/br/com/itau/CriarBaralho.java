package br.com.itau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CriarBaralho {

    public List<Carta> retornaLista(){

        List<Carta> listaDeCartas = new ArrayList<>();

        for(Baralho baralho: Baralho.values()){
            System.out.println(Baralho.values());
            for(Naipe naipe: Naipe.values()){
                System.out.println(Naipe.values());
                Carta carta = new Carta(baralho, naipe);
                listaDeCartas.add(carta);
            }
        }

        return listaDeCartas;
    }

    public Carta sortearCarta(List<Carta> lista) {

        Collections.shuffle(lista);

        for(Carta carta : lista){

            return carta;
        }
        return null;
    }




}
